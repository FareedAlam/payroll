from datetime import datetime

from django.db import models


# Create your models here.
class State(models.Model):
    name = models.CharField(max_length=50, null=False,blank=False, unique=True)
    code = models.CharField(max_length=3, blank=True, null=True)
    is_active = models.BooleanField()

    def __str__(self):
        return self.name


class City(models.Model):
    name = models.CharField(max_length=50, null=False,blank=False, unique=True)
    code = models.CharField(max_length=3, blank=True, null=True)
    is_active = models.BooleanField()
    state_id = models.ForeignKey(State, verbose_name='State', null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Zipcode(models.Model):
    zip_code = models.CharField(max_length=6, null=False,blank=False, unique=True)
    is_active = models.BooleanField()
    city_id = models.ForeignKey(City, verbose_name='City', null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
        return self.zip_code


class StaffType(models.Model):
    type = models.CharField(max_length=30, null=False,blank=False, unique=True)
    is_active = models.BooleanField()

    def __str__(self):
        return self.type


class Department(models.Model):
    name = models.CharField(max_length=30, null=False,blank=False, unique=True)
    is_active = models.BooleanField()

    def __str__(self):
        return self.name


class Project(models.Model):
    name = models.CharField(max_length=50, null=False,blank=False, unique=True)
    hours_worked = models.DecimalField(max_digits=999, decimal_places=2,blank=True, null=True)
    days_worked = models.DecimalField(max_digits=999,decimal_places=2,blank=True, null=True)
#    department_id = models.ForeignKey(Department, verbose_name='department', null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


# class EmpCategory(models.Model):
#     name = models.CharField(max_length=30, null=False,blank=False, unique=True)
#     is_active = models.BooleanField()
#
#     def __str__(self):
#         return self.name


class Employee(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    email = models.CharField(max_length=50, null=True, blank=True)
    mobile = models.CharField(max_length=10, null=True, blank=True)
    home_mobile = models.CharField(max_length=10, blank=True, null=True)
    department_id = models.ForeignKey(Department, verbose_name='department', null=True, blank=True, on_delete=models.CASCADE)
    type_id = models.ForeignKey(StaffType, verbose_name='type', null=True, blank=True, on_delete=models.CASCADE)
    project_id = models.ForeignKey(Project, verbose_name='Project', null=True, blank=True, on_delete=models.CASCADE)
    city_id = models.ForeignKey(City, verbose_name='City', null=True, blank=True, on_delete=models.CASCADE)
    zipcode = models.ForeignKey(Zipcode, verbose_name='zipcode', null=True, blank=True, on_delete=models.CASCADE)
    Notes = models.TextField(max_length=200,null=True,blank=True)

    def __str__(self):
        return self.first_name + ' ' + self.last_name


class Timesheet(models.Model):
    emp_id = models.ForeignKey(Employee, verbose_name='Employee',null=False, blank=False, on_delete=models.CASCADE)
    check_in = models.DateTimeField(default=datetime.now)
    check_out = models.DateTimeField(default=datetime.now)
#    row_time = models.TimeField(blank=True, null=True)
#    round_time = models.DurationField(blank=True, null=True)

    # def __str__(self):
    #     return self.emp_id


class Payroll(models.Model):
    emp_id = models.ForeignKey(Employee, verbose_name='Employee', null=False, blank=False, on_delete=models.CASCADE)
    start_day = models.DateField(null=False,blank=False)
    end_day = models.DateField(null=False,blank=False)
    total_hours = models.DecimalField(decimal_places=2,max_digits=5)
    gross_pay = models.DecimalField(decimal_places=2, max_digits=5)
    deductions = models.DecimalField(decimal_places=2, max_digits=5, null=True, blank=True)
    net_pay = models.DecimalField(decimal_places=2, max_digits=5)

    def __str__(self):
        return self.emp_id
