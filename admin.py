from django.contrib import admin
from .models import StaffType, Employee,  State, City, Zipcode, Project, Timesheet,Department

# Register your models here.
#admin.site.register(EmpCategory)
admin.site.register(State)
admin.site.register(Department)
admin.site.register(Project)


@admin.register(Timesheet)
class TimesheetAdmin(admin.ModelAdmin):
    list_display = ['emp_id', 'check_in', 'check_out', 'project_id']
    search_fields = ['emp_id']
    list_filter = ['project_id']


@admin.register(Employee)
class EmployeeAdmin(admin.ModelAdmin):
    list_display = ['first_name', 'mobile', 'department_id', 'type_id']
    search_fields = ['first_name', 'last_name', 'mobile']
    list_filter = ['department_id', 'type_id', 'city_id', ]


@admin.register(City)
class CityAdmin(admin.ModelAdmin):
    list_display = ['name', 'state_id']
    search_fields = ['name']
    list_filter = ['state_id']


@admin.register(Zipcode)
class ZipcodeAdmin(admin.ModelAdmin):
    list_display = ['zip_code','city_id']
    search_fields = ['zip_code']
    list_filter = ['city_id']


@admin.register(StaffType)
class ParamMasterAdmin(admin.ModelAdmin):
    fields = ['type', 'is_active']
    list_filter = ['is_active']
    search_fields = ['type']
